# StashCalculator

## Description
A simple calculator app that asks for 2 user inputs, and performs one of the following operations:
- Addition
- Substraction
- Multiplication
- Division

The project is called Stash-Calculator since it was used as the base for a demo that explained how the command `git stash` works.

## Project Environment
- VSCode
- .Net version 5.0.400

## Usage
To test out the completed solution, make sure to have VSCode and .Net5 installed, and then do the following:

1. Clone the project 

`git clone https://gitlab.com/NeonShivPatel/stashcalculator.git`

2. Make sure to be in the root directory 

`cd stashcalculator`

3. Go inside the completed-solution branch 

`git checkout completed-solution`

4. Run the project 

`dotnet run --project ./StashCalc/StashCalc.csproj`


## Authors and acknowledgment
The project was made by Shiv Patel
